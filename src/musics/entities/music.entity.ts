import { isEmpty } from 'rxjs';
import { genredetail } from 'src/genre/entities/genredetail.entity';
import { playlistdetail } from 'src/playlists/entities/playlistdetial.entity';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  Timestamp,
  OneToOne,
  OneToMany,
} from 'typeorm';
@Entity()
export class Music {
  @PrimaryGeneratedColumn({ name: 'music_id' })
  id: number;

  @Column({ name: 'music_image', nullable: true })
  musicImage?: string;

  @Column({ name: 'music_name' })
  musicName: string;

  @Column({ name: 'music_longtime', type: 'time' }) // กำหนด เป็น Date แล้วใช้แค่ dateTime ดึงแค่เวลามาใช้ พอ
  musicLongtime: Date;

  @Column({ name: 'num_play' })
  numPlay: number;

  @OneToMany(
    () => playlistdetail,
    (playlistdetailMusic) => playlistdetailMusic.music,
  )
  playlistdetailMusics: playlistdetail[];

  
}
