import { Timestamp } from 'typeorm';

export class CreateMusicDto {
  id: number;
  musicImage?: string;
  musicName: string;
  musicLongtime: Date;
  numPlay: number;
}
