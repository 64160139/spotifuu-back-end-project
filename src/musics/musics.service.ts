import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateMusicDto } from './dto/create-music.dto';
import { UpdateMusicDto } from './dto/update-music.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Music } from './entities/music.entity';
import { Repository } from 'typeorm/repository/Repository';
import { playlistdetail } from 'src/playlists/entities/playlistdetial.entity';

@Injectable()
export class MusicsService {
  music: Music[] = [];
  constructor(
    @InjectRepository(Music)
    private musicsRepository: Repository<Music>,
  ) {
    const newMusic = new Music();
    newMusic.musicImage = '';
    newMusic.musicName = '';
    newMusic.musicLongtime = null;
    newMusic.numPlay = -1;
    this.music.push(newMusic);
  }
  create(createMusicDto: CreateMusicDto) {
    // สร้าง music
    return this.musicsRepository.save(createMusicDto);
  }

  findAll() {
    return this.musicsRepository.find();
  }

  findOne(id: number) {
    return this.musicsRepository.findOne({ where: { id: id } });
  }

  findName(musicName: string) {
    return this.musicsRepository.findOne({ where: { musicName: musicName } });
  }

  async update(id: number, updateMusicDto: UpdateMusicDto) {
    // Update Music ตาม Id
    const music = await this.musicsRepository.findOneBy({ id: id });
    if (!music) {
      throw new NotFoundException();
    }
    const updateMusic = { ...music, ...updateMusicDto };
    return this.musicsRepository.save(updateMusic);
  }

  remove(id: number) {
    // ลบ Music ตาม Id
    return this.musicsRepository.delete(id);
  }
}
