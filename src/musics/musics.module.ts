import { Module } from '@nestjs/common';
import { MusicsService } from './musics.service';
import { MusicsController } from './musics.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Music } from './entities/music.entity';
import { playlistdetail } from 'src/playlists/entities/playlistdetial.entity';
import { PlaylistsService } from 'src/playlists/playlists.service';
import { Playlist } from 'src/playlists/entities/playlist.entity';
import { genredetail } from 'src/genre/entities/genredetail.entity';
import { GenreService } from 'src/genre/genre.service';
import { Genre } from 'src/genre/entities/genre.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      Music,
      playlistdetail,
      Playlist,
      genredetail,
      Genre,
    ]),
  ],
  controllers: [MusicsController],
  providers: [MusicsService, PlaylistsService, GenreService],
  exports: [MusicsService, PlaylistsService],
})
export class MusicsModule {}
