export class CreatePlaylistDto {
    id : number;
    playlistName: string;
    playlistImage?: string;
    userId: number;
}
