import { Injectable, NotFoundException } from '@nestjs/common';
import { CreatePlaylistDto } from './dto/create-playlist.dto';
import { UpdatePlaylistDto } from './dto/update-playlist.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Playlist } from './entities/playlist.entity';
import { Repository } from 'typeorm';
import { playlistdetail } from './entities/playlistdetial.entity';
import { Music } from 'src/musics/entities/music.entity';
import { User } from 'src/users/entities/user.entity';

@Injectable()
export class PlaylistsService {
  constructor(
    @InjectRepository(Playlist)
    private playlistRepository: Repository<Playlist>,
    @InjectRepository(playlistdetail)
    private playlistdetialRepository: Repository<Playlist>,
    @InjectRepository(Music)
    private musiclRepository: Repository<Music>,
  ) {}
  async create(createPlaylistDto: CreatePlaylistDto) {
    return this.playlistRepository.save(createPlaylistDto);
  }

  findAll() {
    return this.playlistRepository.find({
      relations: { user: true, playlistdetials: true },
    });
  }

  findOne(id: number) {
    return this.playlistRepository.findOne({
      where: { id: id },
      relations: { user: true, playlistdetials: true },
    });
  }
  async update(id: number, updatePlaylistDto: UpdatePlaylistDto) {
    // const index = await this.playlisyRepository.findOneBy({ id: id });
    // if (!index) {
    //   throw new NotFoundException();
    // }
    // const updatePlayList = { ...index, ...updatePlaylistDto };
    // return this.playlisyRepository.save(updatePlayList);
  }

  async remove(id: number) {
    return this.playlistRepository.delete(id);
  }
}
