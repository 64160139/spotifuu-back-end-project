import { Entity, PrimaryGeneratedColumn, ManyToOne, JoinColumn } from 'typeorm';
import { Playlist } from './playlist.entity';
import { Music } from 'src/musics/entities/music.entity';
@Entity()
export class playlistdetail {
  @PrimaryGeneratedColumn({ name: 'playlistdetail_id' })
  id: number;

  @ManyToOne(() => Playlist, (playlistId) => playlistId.playlistdetials)
  @JoinColumn({ name: 'playlist_id' })
  playlistId: Playlist;

  @ManyToOne(() => Music, (music) => music.playlistdetailMusics)
  @JoinColumn({ name: 'music_id' })
  music: Music;
}
