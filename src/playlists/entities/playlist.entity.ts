import { User } from 'src/users/entities/user.entity';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  ManyToOne,
  OneToMany,
  JoinColumn,
} from 'typeorm';
import { playlistdetail } from './playlistdetial.entity';
@Entity()
export class Playlist {
  @PrimaryGeneratedColumn({ name: 'playlist_id' })
  id: number;

  @Column({ name: 'playlist_name' })
  name: string;

  @Column({ name: 'playlist_image', nullable: true })
  playlistImage: string;

  @ManyToOne(() => User, (user) => user.playlists)
  @JoinColumn({ name: 'user_id' })
  user: User;

  @OneToMany(
    () => playlistdetail,
    (playlistDetail) => playlistDetail.playlistId,
  )
  playlistdetials: playlistdetail[];
}
