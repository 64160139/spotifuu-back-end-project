import { Module } from '@nestjs/common';
import { PlaylistsService } from './playlists.service';
import { PlaylistsController } from './playlists.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Playlist } from './entities/playlist.entity';
import { playlistdetail } from './entities/playlistdetial.entity';
import { Music } from 'src/musics/entities/music.entity';
import { MusicsService } from 'src/musics/musics.service';
import { User } from 'src/users/entities/user.entity';
import { UsersService } from 'src/users/users.service';

@Module({
  imports: [TypeOrmModule.forFeature([Playlist, playlistdetail, Music, User])],
  controllers: [PlaylistsController],
  providers: [PlaylistsService, MusicsService, UsersService],
  exports: [PlaylistsService, MusicsService, UsersService],
})
export class PlaylistsModule {}
