import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UsersModule } from './users/users.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from './users/entities/user.entity';
import { DataSource } from 'typeorm';
import { MusicsModule } from './musics/musics.module';
import { Music } from './musics/entities/music.entity';
import { PlaylistsModule } from './playlists/playlists.module';
import { Playlist } from './playlists/entities/playlist.entity';

import { GenreModule } from './genre/genre.module';
import { Genre } from './genre/entities/genre.entity';
import { playlistdetail } from './playlists/entities/playlistdetial.entity';
import { AlbumsModule } from './albums/albums.module';
import { Album } from './albums/entities/album.entity';
import { UploadsModule } from './uploads/uploads.module';
import { Upload } from './uploads/entities/upload.entity';

@Module({
  imports: [
    UsersModule,
    MusicsModule,
    TypeOrmModule.forRoot({
      type: 'sqlite',
      // host: 'localhost',
      // port: 3306,
      // username: 'root',
      // password: 'root',
      database: 'Database-Spotifuu.db',
      entities: [User, Music, Playlist, Genre, playlistdetail, Album, Upload],
      synchronize: true,
    }),
    MusicsModule,
    PlaylistsModule,
    GenreModule,
    AlbumsModule,
    UploadsModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {
  constructor(private dataSource: DataSource) {}
}
