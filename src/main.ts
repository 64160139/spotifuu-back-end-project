import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import * as cors from 'cors';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.use(cors()); // เพิ่ม middleware cors ที่กำหนดให้เป็นทุกโดเมน
  await app.listen(3000);
  app.enableCors()
}

bootstrap();
