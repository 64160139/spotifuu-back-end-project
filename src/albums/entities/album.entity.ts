import { User } from 'src/users/entities/user.entity';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  OneToMany,
  ManyToOne,
} from 'typeorm';
@Entity()
export class Album {
  @PrimaryGeneratedColumn({ name: 'album_id' })
  id: number;

  @Column({ name: 'album_name' })
  name: number;

  @ManyToOne(() => User, (user) => user.albums)
  user: User;

  @Column({ name: 'album_image', nullable: true })
  albumImage?: string;
}
