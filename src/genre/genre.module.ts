import { Module } from '@nestjs/common';
import { GenreService } from './genre.service';
import { GenreController } from './genre.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Genre } from './entities/genre.entity';
import { Music } from 'src/musics/entities/music.entity';
import { MusicsService } from 'src/musics/musics.service';
import { genredetail } from './entities/genredetail.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Genre, Music, genredetail])],
  controllers: [GenreController],
  providers: [GenreService, MusicsService, genredetail],
  exports: [GenreService, genredetail, MusicsService],
})
export class GenreModule {}
