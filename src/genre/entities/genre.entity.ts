import { Entity, PrimaryGeneratedColumn, Column, OneToMany } from 'typeorm';
@Entity()
export class Genre {
  @PrimaryGeneratedColumn({ name: 'genre_id' })
  genreId: number;

  @Column({ name: 'genre_name' })
  genreName: string;

  @Column({ name: 'genre_image', nullable: true })
  genreImage?: string;
}
