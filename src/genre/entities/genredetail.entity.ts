import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  OneToMany,
  OneToOne,
  JoinColumn,
  ManyToOne,
} from 'typeorm';
import { Genre } from './genre.entity';
import { Music } from 'src/musics/entities/music.entity';
@Entity()
export class genredetail {
  @PrimaryGeneratedColumn({ name: 'genredetail_id' })
  id: number;

  // @ManyToOne(() => Music, (music) => music.genreDetials)
  // music: Music;

  @OneToOne(() => Genre)
  @JoinColumn({ name: 'genre_id' })
  genre: Genre;
}
