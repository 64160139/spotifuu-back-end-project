export class CreateGenreDto {
  genreId: number;
  genreName: string;
  genreImage?: string;
}
