import { Injectable } from '@nestjs/common';
import { CreateGenreDto } from './dto/create-genre.dto';
import { UpdateGenreDto } from './dto/update-genre.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Genre } from './entities/genre.entity';
import { Repository } from 'typeorm';

@Injectable()
export class GenreService {
  constructor(
    @InjectRepository(Genre)
    private genreRepository: Repository<Genre>,
  ) {}
  create(createGenreDto: CreateGenreDto) {
    return this.genreRepository.save(createGenreDto);
  }

  findAll() {
    return this.genreRepository.find();
  }

  findOne(id: number) {
    return this.genreRepository.findOne({ where: { genreId: id } });
  }

  async update(id: number, updateGenreDto: UpdateGenreDto) {
    // const updategenre = await this.genreRepository.findOne({
    //   where: { genreId: id },
    // });
    // const updateGenre = { ...updategenre, ...updateGenreDto };
    // return this.genreRepository.save(updateGenre);
  }

  remove(id: number) {
    return this.genreRepository.delete(id);
  }
}
