import { User } from 'src/users/entities/user.entity';
import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
@Entity()
export class Upload {
  @PrimaryGeneratedColumn({ name: 'upload_id' })
  id: number;

  @Column({ name: 'categories_upload' })
  categoriesUpload: string;

  @ManyToOne(() => User, (userUpload) => userUpload.uploads)
  @JoinColumn({ name: 'user_id' })
  userUpload: User;
}
