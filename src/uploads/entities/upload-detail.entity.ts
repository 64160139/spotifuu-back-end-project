import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm';

@Entity()
export class UploadDetail {
  @PrimaryGeneratedColumn({ name: 'uploaddetail_id' })
  id: number;

  @Column({ name: 'upload_status' })
  uploadStatus: string;
}
