import { Injectable } from '@nestjs/common';
import { CreateUploadDto } from './dto/create-upload.dto';
import { UpdateUploadDto } from './dto/update-upload.dto';
import { Upload } from './entities/upload.entity';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';

@Injectable()
export class UploadsService {
  constructor(
    @InjectRepository(Upload)
    private uploadRepository: Repository<Upload>,
  ) {}
  create(createUploadDto: CreateUploadDto) {
    return this.uploadRepository.save(createUploadDto);
  }

  findAll() {
    return this.uploadRepository.find({ relations: { userUpload: true } });
  }

  findOne(id: number) {
    return this.uploadRepository.findOne({ where: { id: id } });
  }

  update(id: number, updateUploadDto: UpdateUploadDto) {
    return `This action updates a #${id} upload`;
  }

  remove(id: number) {
    return `This action removes a #${id} upload`;
  }
}
