import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { User } from './entities/user.entity';
import { Playlist } from 'src/playlists/entities/playlist.entity';

@Injectable()
export class UsersService {
  userList: User[] = [];
  constructor(
    @InjectRepository(User)
    private usersRepository: Repository<User>,
  ) {
    const playlist = new Playlist();
    const user = new User();
    user.name = '';
    user.displayName = '';
    user.password = '';
    user.userImage = '';
    user.userEmail = '';
    user.userRole = '';
    this.userList.push(user);
  }
  create(createUserDto: CreateUserDto) {
    return this.usersRepository.save(createUserDto);
  }

  findAll() {
    return this.usersRepository.find({ relations: { playlists: true } });
  }

  findOne(id: number) {
    return this.usersRepository.findOne({
      where: { id },
      relations: { playlists: true, albums: true, uploads: true },
    });
  }
  findByName(name: string) {
    return this.usersRepository.findOne({ where: { name: name } });
  }
  findLogin(name: string, password: string) {
    return this.usersRepository.findOne({
      where: { name: name, password: password },
    });
  }
  findemail(userEmail: string) {
    return this.usersRepository.findOne({
      where: { userEmail: userEmail },
    });
  }
  async update(id: number, updateUserDto: UpdateUserDto) {
    const user = await this.usersRepository.findOneBy({ id: id });
    if (!user) {
      throw new NotFoundException();
    }
    const updateUsers = { ...user, ...updateUserDto };
    return this.usersRepository.save(updateUsers);
  }
  remove(id: number) {
    return this.usersRepository.delete(id);
  }
}
