import { isEmpty } from 'rxjs';
import { Album } from 'src/albums/entities/album.entity';
import { Playlist } from 'src/playlists/entities/playlist.entity';
import { Upload } from 'src/uploads/entities/upload.entity';
import { Entity, PrimaryGeneratedColumn, Column, OneToMany } from 'typeorm';
@Entity()
export class User {
  @PrimaryGeneratedColumn({ name: 'id' })
  id: number;

  @Column({ name: 'user_name' })
  name: string;

  @Column({ name: 'display_name' })
  displayName: string;

  @Column({ name: 'password' })
  password: string;

  @Column({ name: 'user_image', nullable: true })
  userImage: string;

  @Column({ name: 'email' })
  userEmail: string;

  @Column({ name: 'role' })
  userRole: string;

  @OneToMany(() => Playlist, (playlist) => playlist.user)
  playlists: Playlist[];

  @OneToMany(() => Album, (album) => album.user)
  albums: Album[];

  @OneToMany(() => Upload, (upload) => upload.userUpload)
  uploads: Upload[];
}
