import { isEmpty } from 'rxjs';

export class CreateUserDto {
  id: number;

  name: string;

  displayName: string;

  password: string;

  userImage?: string;

  userEmail: string;

  userRole: string;

}
