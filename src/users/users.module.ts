import { Module } from '@nestjs/common';
import { UsersService } from './users.service';
import { UsersController } from './users.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from './entities/user.entity';
import { Playlist } from 'src/playlists/entities/playlist.entity';
import { Album } from 'src/albums/entities/album.entity';
import { Upload } from 'src/uploads/entities/upload.entity';

@Module({
  imports: [TypeOrmModule.forFeature([User, Playlist, Album, Upload])],
  controllers: [UsersController],
  providers: [UsersService],
  exports: [UsersService],
})
export class UsersModule {}
